<?php

declare(strict_types = 1);

/**
 * @file
 * Rules action info hooks and callbacks.
 */

use Drupal\glassboxx_commerce\SendOrderToGlassboxx;

/**
 * Implements hook_rules_action_info().
 */
function glassboxx_commerce_rules_action_info(): array {
  return [
    'glassboxx_commerce_send_order_to_glassboxx' => [
      'label' => t('Send order to Glassboxx'),
      'parameter' => [
        'order' => [
          'type' => 'commerce_order',
          'label' => t('Label'),
        ],
      ],
      'group' => t('Commerce (contrib)'),
      'callbacks' => [
        'execute' => 'glassboxx_commerce_send_order_to_glassboxx',
      ]
    ],
  ];
}

function glassboxx_commerce_send_order_to_glassboxx(\stdClass $order): void {
  SendOrderToGlassboxx::handle($order);
}
