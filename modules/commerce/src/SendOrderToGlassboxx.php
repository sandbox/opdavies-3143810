<?php

declare(strict_types=1);

namespace Drupal\glassboxx_commerce;

use EntityDrupalWrapper;
use EntityMetadataWrapperException;
use Opdavies\Glassboxx\Config;
use Opdavies\Glassboxx\Request\AuthTokenRequest;
use Opdavies\Glassboxx\Request\CustomerRequest;
use Opdavies\Glassboxx\Request\OrderRequest;
use Opdavies\Glassboxx\ValueObject\Customer;
use Opdavies\Glassboxx\ValueObject\CustomerInterface;
use Opdavies\Glassboxx\ValueObject\Order;
use Opdavies\Glassboxx\ValueObject\OrderInterface;
use Opdavies\Glassboxx\ValueObject\OrderItem;
use stdClass;

final class SendOrderToGlassboxx {

  /**
   * The order to send to Glassboxx.
   *
   * @var \stdClass
   */
  private $order;

  /**
   * The wrapped order.
   *
   * @var EntityDrupalWrapper|null
   */
  private $orderWrapper;

  /**
   * The Glassboxx configuration.
   *
   * @var Config
   */
  private $config;

  /**
   * The Glassboxx auth token.
   *
   * @var string|null
   */
  private $authToken;

  public static function handle(stdClass $order): void {
    (new self($order))->send();
  }

  final private function __construct(\stdClass $order) {
    $this->order = $order;
    $this->orderWrapper = new EntityDrupalWrapper('commerce_order', $order);
  }

  private function send(): void {
    $lineItems = $this->extractGlassboxxItems();
    if (empty($lineItems)) {
      return;
    }

    $this->getConfig()->getAuthToken();

    $customer = $this->buildCustomer();
    $customerResponse = $this->makeCustomerRequest($customer);

    $order = $this->buildOrder($customer);
    $orderItems = $this->buildOrderItems($lineItems);
    $orderResponse = $this->makeOrderRequest($order, $orderItems);

    watchdog(
      'glassboxx_commerce',
      'Pushing order %order_id to Glassboxx. Auth token: %auth_token.
      Customer response: %customer_response.
      Order response: %order_response',
      [
        '%order_id' => $order->getOrderNumber(),
        '%auth_token' => $this->authToken,
        '%customer_response' => $customerResponse,
        '%order_response' => $orderResponse,
      ],
      WATCHDOG_DEBUG
    );
  }

  private function extractGlassboxxItems(): array {
    $lineItems = $this->orderWrapper->get('commerce_line_items')->value();

    return array_filter($lineItems, function (stdClass $lineItem): bool {
      return $this->isGlassboxxItem($lineItem);
    });
  }

  private function getConfig(): self {
    $this->config = new Config(
      (int) variable_get(GLASSBOXX_VENDOR_ID),
      variable_get(GLASSBOXX_USERNAME),
      variable_get(GLASSBOXX_PASSWORD)
    );

    return $this;
  }

  private function getAuthToken(): self {
    $this->authToken = (new AuthTokenRequest())
      ->withConfig($this->config)
      ->getToken();

    return $this;
  }

  private function buildCustomer(): CustomerInterface {
    $address = $this->orderWrapper->get('commerce_customer_billing')
      ->get('commerce_customer_address');

    $firstName = $address->get('first_name')->value();
    $lastName = $address->get('last_name')->value();
    $emailAddress = $this->orderWrapper->get('mail')->value();

    return new Customer($firstName, $lastName, $emailAddress);
  }

  private function makeCustomerRequest(CustomerInterface $customer): string {
    return (new CustomerRequest())
      ->forCustomer($customer)
      ->withAuthToken($this->authToken)
      ->withConfig($this->config)
      ->execute();
  }

  private function buildOrder(CustomerInterface $customer): OrderInterface {
    return new Order(
      $customer,
      $this->orderWrapper->get('order_id')
        ->value(),
      $this->orderWrapper->get('commerce_order_total')
        ->get('currency_code')
        ->value()
    );
  }

  private function makeOrderRequest(OrderInterface $order, array $orderItems): string {
    return (new OrderRequest())
      ->forOrder($order)
      ->withOrderItems($orderItems)
      ->withAuthToken($this->authToken)
      ->withConfig($this->config)
      ->execute();
  }

  private function isGlassboxxItem(stdClass $lineItem): bool {
    try {
      return (new EntityDrupalWrapper('commerce_line_item', $lineItem))
        ->get('commerce_product')
        ->get('field_fulfilled_by_glassboxx')
        ->value();
    }
    catch (EntityMetadataWrapperException $e) {
      return FALSE;
    }
  }

  private function buildOrderItems(array $lineItems): array {
    return array_map(function (\stdClass $lineItem): OrderItem {
      $lineItemWrapper = new EntityDrupalWrapper(
        'commerce_line_item',
        $lineItem
      );

      return new OrderItem(
        $lineItemWrapper->get('commerce_unit_price')
          ->get('amount_decimal')
          ->value(),
        $lineItemWrapper->get('commerce_product')
          ->get('sku')
          ->value()
      );
    }, $lineItems);
  }

}
