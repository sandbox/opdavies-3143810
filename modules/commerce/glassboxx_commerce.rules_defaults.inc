<?php

declare(strict_types = 1);

/**
 * Implements hook_default_rules_configuration().
 */
function glassboxx_commerce_default_rules_configuration(): array {
  $items['commerce_add_to_cart_confirmation_message'] = entity_import('rules_config', '{ "rules_send_order_to_glassboxx" : {
    "LABEL" : "Send order to Glassboxx",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "commerce", "glassboxx" ],
    "REQUIRES" : [ "glassboxx_commerce", "commerce_payment" ],
    "ON" : { "commerce_payment_order_paid_in_full" : [] },
    "DO" : [
      { "glassboxx_commerce_send_order_to_glassboxx" : { "order" : [ "commerce-order" ] } }
    ]
  }
}');

  return $items;
}
