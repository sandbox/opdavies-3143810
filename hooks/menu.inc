<?php

/**
 * @file
 * Menu hooks for Glassboxx module.
 */

/**
 * Implements hook_menu().
 */
function glassboxx_menu(): array {
  $items['admin/config/services/glassboxx'] = [
    'title' => t('Glassboxx'),
    'description' => t('Glassboxx settings'),
    'page callback' => 'drupal_get_form',
    'page arguments' => ['glassboxx_settings_form'],
    'access arguments' => ['administer site configuration'],
  ];

  return $items;
}

/**
 * Page callback for the Glassboxx settings form.
 */
function glassboxx_settings_form(): array {
  $form['credentials'] = [
    '#title' => t('Interface credentials'),
    '#description' => t('Enter your interface credentials, found on the <a href="@url">Glassboxx Vendor integration page</a>.', [
      '@url' => 'https://server.glassboxx.co.uk/marketplace/catalog/integration',
    ]),
    '#type' => 'fieldset',
  ];

  $form['credentials'][GLASSBOXX_VENDOR_ID] = [
    '#title' => t('Glassboxx vendor ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get(GLASSBOXX_VENDOR_ID),
    '#element_validate' => [
      'element_validate_integer',
    ],
  ];

  $form['credentials'][GLASSBOXX_USERNAME] = [
    '#title' => t('Glassboxx token username'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get(GLASSBOXX_USERNAME),
  ];

  $form['credentials'][GLASSBOXX_PASSWORD] = [
    '#title' => t('Glassboxx token password'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get(GLASSBOXX_PASSWORD),
  ];

  return system_settings_form($form);
}
